#ifndef BALL_H
#define BALL_H

#include <Godot.hpp>
#include <Area.hpp>
#include "unordered_map"

namespace godot {

class Ball : public Area {
    GODOT_CLASS(Ball, Area)

private:
    float velocity;
    Vector3 direction;

public:
    static void _register_methods();
    Ball();
    ~Ball();
    void _init();

    void _physics_process(float delta);
    void on_area_entered(Variant area);
    Vector3 get_wall_normal(String wall_name);
};

}

#endif