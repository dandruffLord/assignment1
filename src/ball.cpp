#include "ball.h"

using namespace godot;

void Ball::_register_methods() {
    register_method("_physics_process", &Ball::_physics_process);
    register_method("on_area_entered", &Ball::on_area_entered);
}

Ball::Ball() {
}

Ball::~Ball() {
}

void Ball::_init() {
    srand(get_instance_id());
    auto get_rand_value = []() {
        return static_cast<float>(rand()) / static_cast<float>(RAND_MAX) * 2 - 1;
    };
    direction = Vector3(get_rand_value(), get_rand_value(), get_rand_value());
    velocity = .3;
    connect("area_entered", this, "on_area_entered");
}

void Ball::_physics_process(float delta) {
    translate(velocity * direction);
}

void Ball::on_area_entered(Variant area) {
    Ball* other_ball = cast_to<Ball>(area);
    if (other_ball) {
        Vector3 ball_distance = get_global_transform().get_origin() - other_ball->get_global_transform().get_origin();
        ball_distance.normalize();
        direction = direction.bounce(ball_distance);
    }
    else {
        direction = direction.bounce(get_wall_normal(cast_to<Node>(area)->get_name()));
    }
}

Vector3 Ball::get_wall_normal(String wall_name) {
    if (wall_name == "Left") {
        return -Vector3::LEFT;
    }
    else if (wall_name == "Right") {
        return -Vector3::RIGHT;
    }
    else if (wall_name == "Up") {
        return -Vector3::UP;
    }
    else if (wall_name == "Down") {
        return -Vector3::DOWN;
    }
    else if (wall_name == "Forward") {
        return -Vector3::FORWARD;
    }
    else if (wall_name == "Back") {
        return -Vector3::BACK;
    }
    else {
        Godot::print("Unidentified node");
        return Vector3::ZERO;
    }
}