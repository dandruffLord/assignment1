Build Instructions:
Clone the repo.
Initialize all the necessary submodules:
//
cd project2
git submodule update --init --recursive
//
Build the godot-cpp bindings:
//
cd godot-cpp
scons platform=(platform) generate_bindings=yes -j(number of cores)
cd ..
//
Finally, build the project:
//
scons platform=(platform)
//